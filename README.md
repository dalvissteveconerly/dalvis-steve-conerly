Hi, my name is Steve Conerly.

I am a Licensed Professional Counselor and a Licensed Substance Abuse Counselor.



I graduated from East Carolina University in 1992 with a Masters in Counseling Education and from Drew University in 2001, with a Doctorate in Spiritual Guidance.
I have over 19 years of experience working with inpatient and outpatient therapy.


Website: http://www.drgcounseling.org/stevec-bio.html
